//CONFIGURACION DEL APP
 
module.exports = {

  //no tocar es el salt para la contraseña
  SALT: 10,
  TOKEN_SECRET: "secret_testtoken",

  //base de datos name
  URL_BD : "mongodb://localhost/paybk",

  //monto base del dolar
  DOLAR_I : 2000,

  //monto base del euro
  EURO_I : 2500,

  //delay para la actulizacion de la tasa
  DELAY: 30000

};