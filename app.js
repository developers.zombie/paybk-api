const  express = require('express');
const bodyParser = require("body-parser");
const http = require("http");
const socketIo = require("socket.io");
const app = express();
const server = http.createServer(app);
const io = socketIo(server); 
const cors = require('cors');
const api = require("./src/routers/route");
const mongoose = require('mongoose');

const conexion = require('./src/conectBD');

const socket = require('./src/socketConect');

const config = require('./config');

const Divisa = require('./src/models/divisasM');
const VentaDivisa = require('./src/models/ventasM');

let port = process.env.PORT || 5000;

//conexion db
conexion.Conect;
//let db_local = process.env.DB_LOCAL;


app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json({ limit: "300kb" }));
let  corsOptions = {
  origin: '*',
  optionsSuccessStatus: 200 
}

//soporte para cors
app.use(cors(corsOptions));

//usando las rutas
app.use("/api/",  api);



//socket.coneSocket( io );
//test del random
io.on("connection", async ( socket ) => {

  
    const testVentas = await VentaDivisa.find().populate('userId', 'name');

      let ventaData = (testVentas .length > 0) ? testVentas  : {};

      io.emit('venta', ventaData);


      const newDivisa =  await Divisa.find().limit(1);

      //emitimos el test
      io.emit('test',{ 'DOLAR': newDivisa[0].DOLAR  ,'EURO': newDivisa[0].EURO} );


      

/*
  setInterval( async () => {
    try {
  
      const divisa =  await Divisa.find().limit(1);

      let dolar = Math.floor(Math.random() * 5000);
    
      // sacamos el 5% al dolar para sumarlo al euro
      let p = (dolar * 5 ) / 100;
    
      let euro = dolar + p;
    
      let body = {
          DOLAR: dolar,
          EURO: euro
      };
      
      //actulizamos la divisa
      const newDivisa = await Divisa.findByIdAndUpdate( divisa[0]._id , body );

      //emitimos el test
      io.emit('test',{ 'DOLAR': newDivisa.DOLAR  ,'EURO': newDivisa.EURO} );
     
    } catch (error) { console.log(error);}

  },config.DELAY);
 */
});

setInterval( async () => {
  try {

    const divisa =  await Divisa.find().limit(1);

    let dolar = Math.floor(Math.random() * 5000);
  
    // sacamos el 5% al dolar para sumarlo al euro
    let p = (dolar * 5 ) / 100;
  
    let euro = dolar + p;
  
    let body = {
        DOLAR: dolar,
        EURO: euro
    };
    
    //actulizamos la divisa
    const newDivisa = await Divisa.findByIdAndUpdate( divisa[0]._id , body );

    //emitimos el test
    io.emit('test',{ 'DOLAR': newDivisa.DOLAR  ,'EURO': newDivisa.EURO} );
   
  } catch (error) { console.log(error);}

},config.DELAY);




  





// require("dotenv").config();
// process.env.URL_TEMPLATE

server.listen( port , () => console.log(`Servidor Corriendo en http://localhost:${port}`) );



