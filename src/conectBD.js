const mongoose = require("mongoose");

const config = require('../config');

mongoose.set('useCreateIndex', true);

mongoose.set('useFindAndModify', false);


const connection = mongoose.connect(config.URL_BD, {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

const db = mongoose.connection;

db.on("error", console.error.bind(console, "MongoDB connection error:"));

module.exports.Conect = db;



