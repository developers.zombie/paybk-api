const jwt = require('jwt-simple');
const moment = require('moment');
const config =  require('../../config');

exports.Token = function (user){

  let payload = {
    user : user,
    exp: moment().add(10, "days").unix()
  };

  return jwt.encode(payload, config.TOKEN_SECRET);
};



