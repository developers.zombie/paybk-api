const express = require("express");
const api = express.Router();
const userCtrl = require("../controllers/userC");
const authCtrl = require("../controllers/authC");
const divisaCtrl = require("../controllers/divisaC");
const ventaCtrl = require("../controllers/ventaC");
const compraCtrl = require("../controllers/compraC");
const auth = require("../auth");

//USUARIO
api.get("/users", userCtrl.index);
api.get("/user/:id", userCtrl.show);
api.put("/users/:id" , userCtrl.update);
api.delete("/users/:id", auth.Auth , userCtrl.delete);


//VENTA DE DIVISA
api.get("/ventas", ventaCtrl.index);
api.post('/venta', ventaCtrl.venta);
//eliminar antes de comprar la venta
api.delete("/venta/:id",ventaCtrl.delete);

//COMPRA DE DIVISA
api.post("/compra", compraCtrl.compra);


//INICIAr LA TASA DIVISA
api.get("/divisa", divisaCtrl.index);
api.post("/divisa", divisaCtrl.store);



//LOGIN LOGOUT
api.post("/login", authCtrl.login);
api.post("/register",userCtrl.store);
//api.post("/singup", authCtrl.singup);





module.exports = api;



