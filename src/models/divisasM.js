const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const Divisa = new Schema({

    DOLAR: { type: Number },
    EURO: { type: Number }

});



module.exports = mongoose.model("Divisa", Divisa);