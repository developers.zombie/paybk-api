const User = require('../models/usersM');

const VentaDivisa = require('../models/ventasM');


module.exports = {

  //funcion para comcretar las campra
  compra: async (req, res) => {

    //recibimos un objeto con id de la venta , tasa de cambio actual , id comprador
    let { ventaId, userIdComprador, tasaCambio } = req.body.compra;

    //buscar la compra por _id = id_venta
    let vent = await VentaDivisa.find({ _id: ventaId });

    let venta = vent[0];

    //detectar tipo de divisa para sumar  al comprador

    let cantidad_bolivar = (parseInt(venta.dolar) > 0) ?
      (parseInt(venta.dolar) * parseInt(tasaCambio.dolar)) :
      (parseInt(venta.euro) * parseInt(tasaCambio.euro));

    updateBilleterC(venta, cantidad_bolivar , userIdComprador );

    updateBilleterV(venta, cantidad_bolivar);

    await VentaDivisa.deleteOne({ _id: ventaId });

    return res.status(200).json(venta);
  }


}



const updateBilleterV = async (venta, cantidad_bolivar) => {

  //atulizamos la billetera del usuario que vende

  let userVendedor = await User.find({ _id: venta.userId });

  let bm = (parseInt(userVendedor[0].billetera.bolivar) + cantidad_bolivar);

  let nBv = (userVendedor[0].billetera.bolivar - cantidad_bolivar);

  let billeteraVendedor = {
    billetera: {
      dolar: userVendedor[0].billetera.dolar,
      euro: userVendedor[0].billetera.euro,
      bolivar: nBv
    }
  };

  await User.findOneAndUpdate({ _id: userVendedor[0]._id }, { billeteraVendedor }, { new: true });
}


const updateBilleterC = async (venta, cantidad_bolivar , userIdComprador) => {

  //atulizamos la billetera del usuario que compra
  let userComprador = await User.find({ _id: userIdComprador });

  let { dolar, euro, bolivar } = userComprador[0].billetera;

  let nD = (parseInt(venta.dolar) > 0) ? (dolar + parseInt(venta.dolar)) : dolar;
  let nE = (parseInt(venta.euro) > 0) ? (euro + parseInt(venta.euro)) : euro;
  let nB = (bolivar - cantidad_bolivar);

  let billeteraComprador = {
    billetera: {
      dolar: nD,
      euro: nE,
      bolivar: nB
    }
  };

  let usc = await User.findOneAndUpdate({ _id: userIdComprador }, billeteraComprador, { new: true });

 
}






