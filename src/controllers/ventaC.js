const User = require('../models/usersM');

const VentaDivisa = require('../models/ventasM');




module.exports = {

    index(req, res) {

        VentaDivisa.find().populate('userId', 'name').then(ventas => {

            let ventaData = (ventas.length > 0) ? ventas : {};

            res.status(200).json(ventaData);

        }).catch(error => res.status(500).json({ error : 'error'}) );


    },

   venta: async (req, res) => {
        //verificamos si la venta viene en cero
        if (req.body.venta.dolar === 0 && req.body.venta.euro === 0 ) {
            return res.status(500).json({ error : 'error'});
        }

        //buscamos el usuario vendedor y su billetera
        let user  = await User.findById(req.body.venta.userId);

        //se verifica si cuenta con divisa para vender en su billetera
        if (parseInt(user.billetera.dolar) < req.body.venta.dolar || 
            parseInt(user.billetera.euro) < req.body.venta.euro) {
                return res.status(500).json({ error : 'error'});
        }
        
        //todo ok creamos la venta y publicamos la venta de divisa

        let venta = new VentaDivisa({
            dolar: req.body.venta.dolar,
            euro: req.body.venta.euro,
            status: false,
            userId: req.body.venta.userId
        });
       
        let ventaCreada =   await  venta.save();

        //actulizamos la cantidad de divisa en la billetera del usurio
    
        let dolar = parseInt(user.billetera.dolar) - parseInt(ventaCreada.dolar);
    
        let euro =  parseInt(user.billetera.euro) - parseInt(ventaCreada.euro);

        let b =  {
            billetera: {
                dolar: dolar,
                euro: euro,
                bolivar: user.billetera.bolivar
            }
        }
        
        //actulizamos la billetera del usuario
        let userBilleteraUpdate = await User.findOneAndUpdate({ _id : user._id }, b , {new: true}); 

    
        return res.status(200).json({userBilleteraUpdate});
            
    },

   
    delete(req , res){
        return 1;
    }

};


