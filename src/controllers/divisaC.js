const Divisa = require('../models/divisasM');
const config = require('../../config');



module.exports = {

    index(req, res) {

        Divisa.find().then(divisa => {

            res.status(200).json(divisa);

        }).catch(error => console.log(error));

    },
    store(req, res) {
        let divisa = new Divisa({

            DOLAR: config.DOLAR_I,
            EURO: config.EURO_I

        });
        divisa.save().then(divisa => {
            res.status(200).json(divisa);
        }).catch(error => console.log(error));
    }

}
